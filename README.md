# Powershell Commands

## Getting started

This is a quiz to test your knowledge of Powershell Commands.

Answer the questions by editing the file : [TestPowershellCommands.md](/TestPowershellCommands.md)
