# Questions

1. What is Powershell Scripting?

- Type the commands in a text editor
- Save the file with .ps1 extension
- Execute the file in PowerShell
- All of these

2. What are the types of format commands that can be used to Format data?

- Format-Wide
- Format-List
- Format-Table
- All of the above

3. Which of the following command is used to get child items in powershell?

- Get-Child
- Set-Alias
- Get-Command
- Get-ChildItem

4. Variables are the fundamental part of the Windows PowerShell

- TRUE
- FALSE
- Can be true or false
- Can not say

5. In Windows PowerShell, the name of a variable starts with ?
- @
- `#`
- &
- $

6. The name of the variables are?

- case-sensitive
- not case-sensitive
- Both A and B
- None of the above

7. By default, the value of all the variables in a PowerShell is?

- zero
- empty
- null
- one

8. Is variable name $my-variable is valid?

- Invalid variable names
- Valid variable names
- Can be A and B
- Can not say

9. Which operators are used in PowerShell to compare the values

- assignment operators
- logical operators
- redirection operators
- comparison operators

10. The syntax which describe how to use the assignment operators?

- `<assignment-operator> <value>`
- `<assignable-expression> <assignment-operator>`
- `<assignable-expression> <value>`
- `<assignable-expression> <assignment-operator> <value>`

11. The split operator is used to split a string into substrings

- Yes
- No
- Can be yes or no
- Can not say

12. When we need to run a loop at least once, then we use the __________ loop in a PowerShell

- for
- while
- do-while
- foreach

13. The Do-Until loop executes its statements in a code block until the condition is _________

- TRUE
- FALSE
- Can be true or false
- Can not say

14. The _______ statement is used in PowerShell to exit the loop immediately

- Exit
- Break
- Entry
- Continue

15. In PowerShell, the addition and multiplication operator may also be used with the strings, hash tables, and arrays

- TRUE
- FALSE
- Can be true or false
- Can not say